use super::compact_triple::*;
use super::graph::*;
use super::triple::*;
use crate::iter::SortedIterator;
use std::marker::PhantomData;

pub struct GraphIterator<'g, SPO: 'g, OPS: 'g, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
{
    pub graph: &'g GraphData<SPO, OPS>,
    pub pos: usize,
    pub phantom: PhantomData<(T, F)>,
}

impl<'g, SPO, OPS, T, F> Iterator for GraphIterator<'g, SPO, OPS, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
    F: Index<SPO, OPS, T>,
{
    type Item = Triple<'g, SPO, OPS, T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.pos < F::index(self.graph).len() {
            let triple = F::index(self.graph)[self.pos];
            self.pos += 1;
            Some(Triple {
                graph: self.graph,
                triple,
            })
        } else {
            None
        }
    }
}
impl<'g, SPO, OPS, T, F> SortedIterator for GraphIterator<'g, SPO, OPS, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
    F: Index<SPO, OPS, T>,
{
}

pub struct TripleRangeIterator<'g, SPO: 'g, OPS: 'g, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
{
    pub graph: &'g GraphData<SPO, OPS>,
    pub pos: usize,
    pub end: T,
    pub phantom: PhantomData<F>,
}

impl<'g, SPO, OPS, T, F> Iterator for TripleRangeIterator<'g, SPO, OPS, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
    F: Index<SPO, OPS, T>,
{
    type Item = Triple<'g, SPO, OPS, T>;
    fn next(&mut self) -> Option<Self::Item> {
        if self.pos < F::index(self.graph).len() {
            let triple = F::index(self.graph)[self.pos];
            self.pos += 1;
            if triple < self.end {
                Some(Triple {
                    graph: self.graph,
                    triple,
                })
            } else {
                None
            }
        } else {
            None
        }
    }
}
impl<'g, SPO, OPS, T, F> SortedIterator for TripleRangeIterator<'g, SPO, OPS, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
    F: Index<SPO, OPS, T>,
{
}

pub struct SubjectIterator<'g, SPO: 'g, OPS: 'g, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
{
    pub graph: &'g GraphData<SPO, OPS>,
    pub pos: usize,
    pub phantom: PhantomData<(T, F)>,
}

impl<'g, SPO, OPS, T, F> Iterator for SubjectIterator<'g, SPO, OPS, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
    F: Index<SPO, OPS, T>,
{
    type Item = BlankNodeOrIRI<'g, SPO, OPS>;
    fn next(&mut self) -> Option<Self::Item> {
        let len = F::index(self.graph).len();
        if self.pos < len {
            let triple = F::index(self.graph)[self.pos];
            let subject = triple.subject();
            // advance position until the next subject
            self.pos += 1;
            while self.pos < len && subject == F::index(self.graph)[self.pos].subject() {
                self.pos += 1;
            }
            Some(triple_to_subject(&triple, self.graph))
        } else {
            None
        }
    }
}
impl<'g, SPO, OPS, T, F> SortedIterator for SubjectIterator<'g, SPO, OPS, T, F>
where
    SPO: CompactTriple<u32>,
    OPS: CompactTriple<u32>,
    T: CompactTriple<u32>,
    F: Index<SPO, OPS, T>,
{
}
